#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import webapp2
import jinja2
import os
import re
import logging
import random
import hashlib
import hmac
import json

from string import letters
from datetime import datetime, timedelta
from urllib import urlencode
from google.appengine.api import urlfetch
from google.appengine.api import memcache
from google.appengine.ext import db

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape = True)

secret = 'imsosecretlol'

#captcha vars
PRIVATE_KEY = "6LeuvNUSAAAAAOlW6d8JjAwBqvFFFQxD80-ElFVE"
VERIFY_URL = "http://www.google.com/recaptcha/api/verify"

#stores request in cache
def age_set(key, val):
    save_time = datetime.utcnow()
    memcache.set(key,(val, save_time))

#retrieves request from cache
def age_get(key):
    r = memcache.get(key)
    if r:
        val, save_time = r
        age = (datetime.utcnow() - save_time).total_seconds()
    else:
        val, age = None, 0
    return val, age

def age_str(age):
    s = 'Queried %s seconds ago'
    age = int(age)
    if age == 1:
        s = s.replace('seconds', 'second')
    return s % age

def query_db(update = False,key = 'top'):
    entries, age = age_get(key)
    if entries is None or update:
        entries = db.GqlQuery("SELECT * FROM Entry ORDER BY created DESC LIMIT 10")
        entries = list(entries)
        age_set(key, entries)
    return entries, age

def query_user_submissions(update = False, user = 'NULL', loc = 'Entry'):
    key = loc + user
    entries, age = age_get(key)
    query = "SELECT * FROM %s WHERE author='%s' ORDER BY created DESC" % (loc,user)
    if entries is None or update:
        entries = db.GqlQuery(query)
        entries = list(entries)
        age_set(key, entries)
    return entries, age

def query_comments(update = False, post_id = '0'):
    key = 'COMMENTS_' + post_id
    query = "SELECT * FROM Comment WHERE parent_id='%s' ORDER BY created DESC" % post_id
    entries, age = age_get(key)
    if entries is None or update:
        entries = db.GqlQuery(query)
        entries = list(entries)
        age_set(key, entries)
    
    return entries, age

def make_secure_val(val):
    return '%s|%s' % (val, hmac.new(secret, val).hexdigest())

def check_secure_val(secure_val):
    val = secure_val.split('|')[0]
    if secure_val == make_secure_val(val):
        return val

#checks valid inputs
USER_RE = re.compile(r"^[a-zA-Z0-9_-]{3,20}$")
def valid_username(username):
    return username and USER_RE.match(username)

PASS_RE = re.compile(r"^.{3,20}$")
def valid_password(password):
    return password and PASS_RE.match(password)

EMAIL_RE = re.compile(r'^[\S]+@[\S]+\.[\S]+$')
def valid_email(email):
    return not email or EMAIL_RE.match(email)

class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))
        
    def get_profile(self):
        user_id = self.request.cookies.get('user_id')
        if user_id:
            username = check_secure_val(user_id)
        else:
            return None
        
        if username:
            name = User.get_by_id(int(username))
            return name
        else:
            return None
        
    def increment_post_count(self):
        user = self.get_profile()
        user.post_count += 1
        user.put()
        
    def get_user(self):
        user_id = self.request.cookies.get('user_id')
        if user_id:
            username = check_secure_val(user_id)
        else:
            return None
        
        if username:
            name = User.get_by_id(int(username))
            if name:
                return name.username
            else:
                return None
        else:
            return None
        
    def render_json(self, d):
        json_txt = json.dumps(d)
        self.response.headers['Content-Type'] = 'application/json; charset=UTF-8'
        self.write(json_txt)
        
    def set_secure_cookie(self, name, val):
        cookie_val = make_secure_val(val)
        remember = self.request.get("remember", default_value="no")
        logging.error(remember)
        if remember == "yes":
            self.response.headers.add_header(
                'Set-Cookie',
                '%s=%s; expires = Tue, 07 Jan 2025 11:48:41 GMT; Path=/' % (name, cookie_val))            
        else:    
            self.response.headers.add_header(
                'Set-Cookie',
                '%s=%s; Path=/' % (name, cookie_val))

    def read_secure_cookie(self, name):
        cookie_val = self.request.cookies.get(name)
        return cookie_val and check_secure_val(cookie_val)
    
    def set_previous_page(self, page):
        self.response.headers.add_header('Set-Cookie', 'last_visited=%s; Path =/' % str(page))
    
    def get_previous_page(self):
        previous = self.request.cookies.get('last_visited')
        if not previous:
            return '/'
        else:
            return previous
    
    def login(self, user):
        self.set_secure_cookie('user_id', str(user.key().id()))

    def logout(self):
        self.response.headers.add_header('Set-Cookie', 'user_id=; Path=/')
        
# user stuff
def make_salt(length = 5):
    return ''.join(random.choice(letters) for x in xrange(length))

def make_pw_hash(name, pw, salt = None):
    if not salt:
        salt = make_salt()
    h = hashlib.sha256(name + pw + salt).hexdigest()
    return '%s,%s' % (salt, h)

def valid_pw(name, password, h):
    salt = h.split(',')[0]
    return h == make_pw_hash(name, password, salt)

# data classes
class User(db.Model):
    username = db.StringProperty(required = True)
    pw_hash = db.StringProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)
    email = db.StringProperty()
    type = db.StringProperty(required = True)
    post_count = db.IntegerProperty(required = True)
    
    @classmethod
    def by_name(cls, name):
        u = User.all().filter('username =', name).get()
        return u
    
class Comment(db.Model):
    content = db.TextProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)
    author = db.StringProperty(required = True)
    author_id = db.StringProperty()
    parent_id = db.StringProperty(required = True)
    
    def as_dict(self):
        time_fmt = '%c'
        d = {'content': self.content,
             'created': self.created.strftime(time_fmt),
             'author':self.author,
             'parent_id': self.parent_id}
        return d
    
class Entry(db.Model):
    subject = db.StringProperty(required = True)
    content = db.TextProperty(required = True)
    created = db.DateTimeProperty(auto_now_add = True)
    last_modified = db.DateTimeProperty(auto_now = True)
    author_id = db.StringProperty()
    author = db.StringProperty()
       
    def as_dict(self):
        time_fmt = '%c'
        d = {'subject': self.subject,
             'content': self.content,
             'author':self.author,
             'created': self.created.strftime(time_fmt),
             'last_modified': self.last_modified.strftime(time_fmt)}
        return d

#handlers for main content
class PermalinkHandler(Handler):
    def get(self,post_id):
        post_key = 'POST_' + post_id
        comment_key = 'COMMENTS_' + post_id
        username = self.get_profile()
        user = self.get_user()
        e, age = age_get(post_key)
        comments, age = age_get(comment_key)
        
        #entry not found in cache
        if not e:
            e = Entry.get_by_id(int(post_id))
            age_set(post_key, e)
            age = 0
            
        if not comments:
            comments, age = query_comments(update = True, post_id = post_id)
            #no comments to begin with, return empty list
            if not comments:
                comments = []
            
        if e:
            match = user == e.author
            self.set_previous_page('/' + post_id)
            self.render("permalink.html", username=username, user = user, e = e, comments = comments, match = match,post_id = post_id,age = age_str(age), error = "")
        else:
            #entry doesn't exist at all
            self.error(404)
            
    def post(self,post_id):
        post_key = 'POST_' + post_id
        comment_key = 'COMMENTS_' + post_id
        content = self.request.get("comment")
        user = self.get_user()
        username = self.get_profile()
        e, age = age_get(post_key)
        comments, age = age_get(comment_key)
        match = user == e.author
        
        logging.error('content value: %s' % content)
        if len(content) == 0:
            error = "Your comment is empty."
            self.render("permalink.html", username=username, user = user, e = e, comments = comments, match = match,post_id = post_id,age = age_str(age), error = error)
            return
        
        if len(content) > 400:
            error = 'The comment can not exceed 400 characters'
            self.render("permalink.html", username = username, user = user, e = e, comments = comments, match = match,post_id = post_id,age = age_str(age), error = error)
        else:
            author_id = str(username.key().id())
            c = Comment(content = content, author = user, parent_id = post_id, author_id = author_id)
            c.put()
            self.increment_post_count()
            query_comments(update = True, post_id = post_id)
            self.redirect('/%s/' % post_id)
            
class Delete(Handler):
    def get(self, post_id):
        e = Entry.get_by_id(int(post_id))
        profile = self.get_profile()
        self.set_previous_page('/')
        
        if e:
            if e.author == profile.username or profile.type == '2' or profile.type == '3':
                #decrements post count
                original_poster = User.get_by_id(int(e.author_id))
                original_poster.post_count -= 1
                original_poster.put()
                #deletes the post
                e.delete()
                #gets rid of comments
                comments = query_comments(update = True, post_id = post_id)[0]
                for comment in comments:
                    comment.delete()
                query_comments(update = True)
 
                #profile.post_count -= 1
                #profile.put()
                query_db(update = True)
                self.redirect('/')
            else:
                self.redirect('/')
        else:
            self.error(404)
            
class Edit(Handler):    
    def get(self, post_id):
        post_key = 'POST_' + post_id
        user = self.get_user()
        username = self.get_profile()
        e, age = age_get(post_key)
          
        if not e:
            e = Entry.get_by_id(int(post_id))
            age_set(post_key, e)
            age = 0
        
        if e:
            if username.type == '2' or username.type == '3':
                content = e.content
                content = content.replace('<br>', '\n')
                self.render("edit.html",e = e, content = content, username = username, user = user,age = age_str(age))
            elif user != e.author:
                self.redirect('/')
            else:
                content = e.content
                content = content.replace('<br>', '\n')
                self.render("edit.html",e = e, content = content, username = username, user = user,age = age_str(age))
        else:
            self.error(404)
            
    def post(self, post_id):
        post_key = 'POST_' + post_id
        e = Entry.get_by_id(int(post_id))
        user = self.get_user()
        username = self.get_profile()
        subject = self.request.get("subject")
        content = self.request.get("content")
        
        if subject and content:
            content = content.replace('\n', '<br>')
            e.subject = subject
            e.content = content
            e.put()
            age_set(post_key, e)
            query_db(update = True)
            self.redirect('/%s/' % post_id)
        else:
            error = "We require both a subject and content!"
            self.render("post.html", user = user, username = username, error = error, subject = subject, content = content)
                                    
class MainHandler(Handler):        
    def render_front(self, subject = "", content = ""):
        self.set_previous_page('/')
        username = self.get_profile()
        if username:
            user = username.username
        else:
            user = None
        entries, age = query_db()
        self.render("front.html", subject = subject, username = username, content = content, entries = entries, user = user, age = age_str(age))
        
    def get(self):
        self.render_front()

#json handlers
class MainJson(Handler):
    def get(self):
        entries = query_db()[0]
        return self.render_json([e.as_dict() for e in entries])
        
class PlinkJson(Handler):
    def get(self,post_id):
        post = Entry.get_by_id(int(post_id))
        comments = query_comments(update = True, post_id = post_id)[0]
        
        if not comments:
            comments = []
            
        if post:
            #post of each entry is at position 0 of list
            self.render_json([post.as_dict()] + [comment.as_dict() for comment in comments])
        else:
            self.error(404)

#new post            
class PostHandler(Handler):
    def get(self):
        self.set_previous_page('/newpost')
        user = self.get_user()
        username = self.get_profile()
        error = ""
        #not logged in
        if not user:
            self.redirect('/login')               
        self.render("post.html", username = username, subject = "", content = "", error = error, user = user)
        
    def post(self):
        subject = self.request.get("subject")
        content = self.request.get("content")
        author = self.get_user()     
        username = self.get_profile() 
        
        if not author:
            author = 'Guest'
            
        if subject and content: 
            #escape breaks
            author_id = None
            content = content.replace('\n', '<br>')
            
            if username:
                author_id = str(username.key().id())
                self.increment_post_count()
                
            e = Entry(subject = subject, content = content, author = author,author_id = author_id)
            e.put() 
            query_db(update = True)
            x = str(e.key().id())
            self.redirect('/%s/' % x)
        else:
            error = "We require both a subject and content!"
            user = author
            self.render("post.html", username = username, error = error, subject = subject, content = content, user = user)  
             
            
class Signup(Handler):
    def get(self):
        if self.get_user():
            self.set_previous_page('/')
            self.redirect('/')
        else:
            self.render("signup-form.html", error_username = "", error_password = "", error_verify = "", error_email = "", username = "", email = "")
        
    def post(self):
        have_error = False
        username = self.request.get('username')
        password = self.request.get('password')
        verify = self.request.get('verify')
        email = self.request.get('email')
        recaptcha_challenge_field = self.request.get("recaptcha_challenge_field")
        recaptcha_response_field = self.request.get("recaptcha_response_field")
        remoteIp = self.request.remote_addr
        
        params = dict(username = username,
                      email = email)
        
        data = {"privatekey": PRIVATE_KEY,
                "remoteip": remoteIp,
                "challenge": recaptcha_challenge_field,
                "response": recaptcha_response_field}
        
        response = urlfetch.fetch(url=VERIFY_URL,payload=urlencode(data),method="POST")
        captcha_ok = True if response.content.split("\n")[0] == "true" else False
        logging.error("First line: %s " % response.content.split("\n")[0])
        logging.error("Valid: %s" % captcha_ok)
        
        if not captcha_ok:
            logging.error("Error message: %s " % response.content.split("\n")[1] )
            params['captcha_msg'] = "Invalid captcha"
            have_error = True
        
        if not valid_username(username):
            params['error_username'] = "That's not a valid username."
            have_error = True

        if not valid_password(password):
            params['error_password'] = "That wasn't a valid password."
            have_error = True
        elif password != verify:
            params['error_verify'] = "Your passwords didn't match."
            have_error = True

        if not valid_email(email):
            params['error_email'] = "That's not a valid email."
            have_error = True

        if have_error:
            self.render('signup-form.html', **params)
        else:
            i = User.by_name(username)
            if i:
                msg = 'That user already exists.'
                self.render('signup-form.html', error_username = msg)
            else:
                pw_hash = make_pw_hash(username,password)
                u = User(username = username, pw_hash = pw_hash, email = email, type = '0', post_count = 0)
                u.put()
                self.login(u)
                self.redirect('/welcome')
            
class Welcome(Handler):
    def get(self):
        user_id = self.request.cookies.get('user_id')
        username = check_secure_val(user_id)
        profile = self.get_profile()
        
        if username:
            name = User.get_by_id(int(username))
            self.render('welcome.html', profile = profile, username = name.username, user = self.get_user())
        else:
            self.redirect('/signup')
            
#login/logout            
class Login(Handler):
    def get(self):
        previous_page = self.get_previous_page()
        user = self.get_user()

        if user:
            self.redirect(previous_page)
        else:
            self.render('login.html', username = "", error_login = "", )
            
    def post(self):
        previous_page = self.get_previous_page()
        username = self.request.get('username')
        password = self.request.get('password')
        error_login = 'Invalid login.'

        
        u = User.by_name(username)
        if u and valid_pw(username, password, u.pw_hash) and u.type != '-1':
            self.login(u)
            self.redirect(previous_page)
        else:
            if u:
                if u.type == '-1':
                    error_login = 'This account has been banned!'
            self.render('login.html',username = username, error_login = error_login)
            
class UserHandler(Handler):
    def get_account_type(self, name):
        if name.type == '0':
            return 'Member'
        elif name.type == '1':
            return 'Admin'
        elif name.type == '2':
            return 'Executive Member'
        elif name.type == '3':
            return 'Founder'
        elif name.type == '-1':
            return 'Banned'
        else:
            return 'NULL'
    
    def get(self, user_id):
        user = self.get_user()
        username = User.get_by_id(int(user_id))
        if username:
            self.set_previous_page('/user/' + user_id)
            type = self.get_account_type(username)
            self.render('profile.html',user = user, username = username, type = type)
        else:
            self.error(404)

class UserPosts(Handler):
    def get(self,user_id):
        username = User.get_by_id(int(user_id))
        profile = self.get_profile()
        
        if username:
            self.set_previous_page('/user/%s/posts' % user_id)
            user = self.get_user()
            entries, age = query_user_submissions(update = True, user = username.username, loc = 'Entry')
            if not entries:
                entries = []
            self.render("UserPosts.html", username = username, profile = profile, entries = entries, user = user, age = age_str(age))
        else:
            self.error(404)

        
class UserComments(Handler):
    def get(self,user_id):
        username = User.get_by_id(int(user_id))
        profile = self.get_profile()
        
        if username:
            self.set_previous_page('/user/%s/comments' % user_id)
            user = self.get_user()
            comments, age = query_user_submissions(update = True, user = username.username, loc = 'Comment')
            if not comments:
                comments = []
            self.render("UserComments.html", username = username, profile = profile, comments = comments, user = user, age = age_str(age))
        else:
            self.error(404)     
          
class Logout(Handler):
    def get(self):
        previous_page = self.get_previous_page()
        self.logout()
        self.redirect(previous_page)
        
class Flush(Handler):
    def get(self):
        memcache.flush_all()
        self.redirect('/')
        
app = webapp2.WSGIApplication([('/', MainHandler),
                               ('/.json/?', MainJson),
                               ('/([0-9]+)/.json/?',PlinkJson),
                               ('/newpost/?', PostHandler),
                               ('/([0-9]+)/?', PermalinkHandler),
                               ('/([0-9]+)/edit/?', Edit),
                               ('/([0-9]+)/delete/?', Delete),
                               ('/user/([0-9]+)/?', UserHandler),
                               ('/user/([0-9]+)/posts/?', UserPosts),
                               ('/user/([0-9]+)/comments/?', UserComments),
                               ('/signup/?', Signup),
                               ('/welcome/?', Welcome),
                               ('/login/?',Login),
                               ('/flush/?',Flush),
                               ('/logout/?',Logout)],
                              debug=True)
